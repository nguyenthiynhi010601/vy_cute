<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Social; //sử dụng model Social
use Socialite; //sử dụng Socialite
use App\Login; //sử dụng model Login
use Carbon\Carbon;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

session_start();

class trangchuController extends Controller
{
    public function dangnhap()
    {
        return view('login');
    }
    public function kiemtra_dangnhap(Request $request)
    {
        $taikhoan = $request->username;
        $matkhau = $request->password;

        $data = DB::table('canboquanly')->where('tenTaiKhoan', $taikhoan)->where('matKhau', $matkhau)->first();

        if ($data) {
            $khoa = $data->trangThaiTaiKhoan;
            if ($khoa == 1) {
                
                    Session::put('nguoidung_name', $data->hoTenCB);
                    Session::put('nguoidung_id', $data->maCanBo);

                    // return Redirect::to('/khachhang');
                    return Redirect::to('/');
            } else {
                Session::put('khoa', 'Tài khoản đã bị khóa');
                return Redirect::to('/dangnhap');
            }
        } else {
            Session::put('dn', 'Tài khoản hoặc mật khẩu sai. Vui lòng nhập lại');
            return Redirect::to('/dangnhap');
        }
    }
    public function dangxuat()
    {
        Session::put('nguoidung_name', null);
        Session::put('nguoidung_id', null);

        return Redirect::to('/dangnhap');
    }
}