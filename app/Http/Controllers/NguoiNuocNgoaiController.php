<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NguoiNuocNgoaiController extends Controller
{
    public function homeNNG()
    {
        return view('nguoinn.khaibaothongtincanhan');
    }
    public function khaibaonhapcanh()
    {
        return view('nguoinn.khaibaonhapcanh');
    }
}