<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Social; //sử dụng model Social
use Socialite; //sử dụng Socialite
use App\Login; //sử dụng model Login
use Carbon\Carbon;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class NguoiQuanLyController extends Controller
{
    public function index()
    {
        return view('layout');
    }
    public function thongbao()
    {
        $tk = DB::table('nguoinuocngoai')->get();
        // echo '<pre>';
        // print_r($ctttkhaibao);
        // echo '</pre>';
        // exit;
        $quanly_tk = view('nguoiql.thongbao')->with('tk',$tk);
        return view('layout')->with('nguoiql.thongbao',$quanly_tk);
    }
    public function thongtinkhaibao()
    {
        $ttkhaibao = DB::table('nguoinuocngoai')
        ->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
        ->where('trangThaiDuyet','Duyệt') ->orWhere ('trangThaiDuyet','Không được duyệt')
        ->paginate(5);
        // echo '<pre>';
        // print_r($ttkhaibao) ;
        // echo '</pre>';
        // exit;
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibao') -> with('thongtinkhaibao',$ttkhaibao);
        return view('layout') -> with('nguoiql.thongtinkhaibao',$quanly_ttkhaibao);
    }
    public function thongtinkhaibaocd()
    {
        $ttkhaibao = DB::table('nguoinuocngoai')
        ->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
        ->where('trangThaiDuyet','Chưa duyệt')
        ->paginate(5);
        // echo '<pre>';
        // print_r($ttkhaibao) ;
        // echo '</pre>';
        // exit;
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibaocd') -> with('thongtinkhaibao',$ttkhaibao);
        return view('layout') -> with('nguoiql.thongtinkhaibaocd',$quanly_ttkhaibao);
    }
    public function thongtinkhaibaokdd()
    {
        $ttkhaibao = DB::table('nguoinuocngoai')
        ->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
        ->where('trangThaiDuyet','Không được duyệt')
        ->paginate(5);
        // echo '<pre>';
        // print_r($ttkhaibao) ;
        // echo '</pre>';
        // exit;
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibaokdd') -> with('thongtinkhaibao',$ttkhaibao);
        return view('layout') -> with('nguoiql.thongtinkhaibaokdd',$quanly_ttkhaibao);
    }
    public function thongtinnhapcanh()
    {
        $ttnhapcanh = DB::table('khaibaonhapcanh')
        ->where('trangThaiDuyet','Duyệt') ->orWhere ('trangThaiDuyet','Không được duyệt')
        ->paginate(5);
        // echo '<pre>';
        // print_r($ttkhaibao) ;
        // echo '</pre>';
        // exit;
        $quanly_ttnhapcanh = view('nguoiql.thongtinnhapcanh') -> with('thongtinnhapcanh',$ttnhapcanh);
        return view('layout') -> with('nguoiql.thongtinkhaibao',$quanly_ttnhapcanh);
    }
    public function thongtinnhapcanhcd()
    {
        $ttnhapcanh = DB::table('khaibaonhapcanh')
        ->where('trangThaiDuyet','Chưa duyệt')
        ->paginate(5);
        // echo '<pre>';
        // print_r($ttnhapcanh) ;
        // echo '</pre>';
        // exit;
        $quanly_ttnhapcanh = view('nguoiql.thongtinnhapcanhcd') -> with('thongtinnhapcanh',$ttnhapcanh);
        return view('layout') -> with('nguoiql.thongtinnhapcanhcd',$quanly_ttnhapcanh);
    }
    public function ctttkhaibao($maHoChieu)
    {
        $ctttkhaibao = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
        ->where('maHoChieu',$maHoChieu)->get();
        // echo '<pre>';
        // print_r($ctttkhaibao);
        // echo '</pre>';
        // exit;
        $quanly_ctttkhaibao = view('nguoiql.ctthongtinkhaibao')->with('ctttkhaibao',$ctttkhaibao);
        return view('layout')->with('nguoiql.ctthongtinkhaibao',$quanly_ctttkhaibao);
    }
    public function ctttnhapcanh($maXacNhan)
    {
        $ctttnhapcanh = DB::table('khaibaonhapcanh')
        ->where('maXacNhan',$maXacNhan)->get();
        // echo '<pre>';
        // print_r($ctttkhaibao);
        // echo '</pre>';
        // exit;
        $quanly_ctttnhapcanh = view('nguoiql.ctthongtinnhapcanh')->with('ctttnhapcanh',$ctttnhapcanh);
        return view('layout')->with('nguoiql.ctthongtinnhapcanh',$quanly_ctttnhapcanh);
    }
    public function cntrangthaiduyet($maHoChieu)
    {
        $maCB = Session::get('nguoidung_id');
        $data = array();
        
        $data['trangThaiDuyet'] = 'Duyệt';
        $data['ngayDuyet'] = Carbon::now();
        $data['maCanBo'] = $maCB;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        DB::table('nguoinuocngoai')
        ->where('maHoChieu',$maHoChieu)->update($data);
        
        $madl = DB::table('nguoinuocngoai')
        ->where('maHoChieu',$maHoChieu)->get('maHoChieu');
        foreach ($madl as $key => $value) {
            $maHC = $value->maHoChieu;
        }
        // echo '<pre>';
        // print_r($ma);
        // echo '</pre>';
        // exit;
        Session::put('message', 'Duyệt');
        return Redirect::to('/captaikhoan/'.$maHC);
    }
    public function cntrangthaiduyetkb($maXacNhan)
    {
        $maCB = Session::get('nguoidung_id');
        $data = array();
        
        $data['trangThaiDuyet'] = 'Duyệt';
        $data['ngayDuyet'] = Carbon::now();
        $data['maCanBo'] = $maCB;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        DB::table('khaibaonhapcanh')
        ->where('maXacNhan',$maXacNhan)->update($data);
        // echo '<pre>';
        // print_r($ma);
        // echo '</pre>';
        // exit;
        Session::put('message', 'Duyệt');
        return Redirect::to('/thongtinnhapcanh');
    }
    public function cntrangthaikhduyet($maHoChieu)
    {
        $maCB = Session::get('nguoidung_id');
        $data = array();
        $data['trangThaiDuyet'] = 'Không được duyệt';
        $data['ngayDuyet'] = Carbon::now();
        $data['maCanBo'] = $maCB;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        DB::table('nguoinuocngoai')
        ->where('maHoChieu',$maHoChieu)->update($data);
        
        Session::put('message', 'Không được duyệt');
        return Redirect::to('/thongtinkhaibao');
    }
    public function cntrangthaikhduyetkb($maXacNhan)
    {
        $maCB = Session::get('nguoidung_id');
        $data = array();
        $data['trangThaiDuyet'] = 'Không được duyệt';
        $data['ngayDuyet'] = Carbon::now();
        $data['maCanBo'] = $maCB;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        DB::table('khaibaonhapcanh')
        ->where('maXacNhan',$maXacNhan)->update($data);
        
        Session::put('message', 'Không được duyệt');
        return Redirect::to('/thongtinnhapcanh');
    }
    
    public function captaikhoan($maHoChieu)
    {
        $tk = DB::table('nguoinuocngoai')->where('maHoChieu',$maHoChieu)->get();
        // echo '<pre>';
        // print_r($ctttkhaibao);
        // echo '</pre>';
        // exit;
        $quanly_tk = view('nguoiql.captaikhoan')->with('tk',$tk);
        return view('layout')->with('nguoiql.captaikhoan',$quanly_tk);
    }
    public function luutaikhoan(Request $request, $maHoChieu) {
        
        $data = array();
        $data['tenTaiKhoan'] = $request -> taikhoan;
        $data['matKhau'] = $request -> matkhau;
        DB::table('nguoinuocngoai')->where('maHoChieu',$maHoChieu)->update($data);
        Session::put('message','Lưu tài khoản thành công');
        return Redirect::to('/thongtinkhaibao');
        print('nhi');
    }
    public function luuthongbao(Request $request) {
        $maCB = Session::get('nguoidung_id');
        $data = array();
        $data['maHoChieu'] = $request->manguoinhan;
        $data['maCanBo'] = $maCB;
        $data['tieuDe'] = $request->tieude;
        $data['noiDung'] = $request->noidung;
        $data['ngayGui'] = Carbon::now();
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // exit;
        if($data['tieuDe']  == '' || $data['noiDung'] == '') {
            Session::put('msg','Nhập đầy đủ tiêu đề và nội dung');
        }
        else{
            
            DB::table('thongbao')->insert($data);
        }
        
        Session::put('message','Gửi thông báo thành công');
        return Redirect::to('/thongbao');
        print('nhi');
    }
    public function timkiem(Request $request)
    {
        $key = $request->tk;
        $search = DB::table('nguoinuocngoai')
            ->where('maHoChieu', 'like', '%' . $key . '%')->orWhere('hoTen', 'like', '%' . $key . '%')->get();
        // echo '<pre>';
        // print_r($search);
        // echo '</pre>';
        // exit;
        $distinct1 = array();
        if($key == '') {
            $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')->paginate(5);
        // echo '<pre>';
        //         print_r($distinct1);
        //         echo '</pre>';
        //         exit;
        } else {
            foreach ($search as $key => $value) {
                $ma = $value->maHoChieu;
                $ten = $value->hoTen;
                // echo $orders_id;
                $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
            ->where('maHoChieu',$ma)->orWhere('hoTen',$ten)->paginate(5);
                
            }
        }
        
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibao') -> with('thongtinkhaibao',$distinct1);
        return view('layout') -> with('nguoiql.thongtinkhaibao',$quanly_ttkhaibao);
    }
    public function timkiemcd(Request $request)
    {
        $key = $request->tk;
        $search = DB::table('nguoinuocngoai')
            ->where('maHoChieu', 'like', '%' . $key . '%')->orWhere('hoTen', 'like', '%' . $key . '%')->get();
        // echo '<pre>';
        // print_r($search);
        // echo '</pre>';
        // exit;
        $distinct1 = array();
        if($key == '') {
            $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')->paginate(5);
        // echo '<pre>';
        //         print_r($distinct1);
        //         echo '</pre>';
        //         exit;
        } else {
            foreach ($search as $key => $value) {
                $ma = $value->maHoChieu;
                $ten = $value->hoTen;
                // echo $orders_id;
                $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
            ->where('maHoChieu',$ma)->orWhere('hoTen',$ten)->paginate(5);
                
            }
        }
        
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibaocd') -> with('thongtinkhaibao',$distinct1);
        return view('layout') -> with('nguoiql.thongtinkhaibaocd',$quanly_ttkhaibao);
    }
    public function timkiemkdd(Request $request)
    {
        $key = $request->tk;
        $search = DB::table('nguoinuocngoai')
            ->where('maHoChieu', 'like', '%' . $key . '%')->orWhere('hoTen', 'like', '%' . $key . '%')->get();
        // echo '<pre>';
        // print_r($search);
        // echo '</pre>';
        // exit;
        $distinct1 = array();
        if($key == '') {
            $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')->paginate(5);
        // echo '<pre>';
        //         print_r($distinct1);
        //         echo '</pre>';
        //         exit;
        } else {
            foreach ($search as $key => $value) {
                $ma = $value->maHoChieu;
                $ten = $value->hoTen;
                // echo $orders_id;
                $distinct1 = DB::table('nguoinuocngoai')->join('quoctich', 'quoctich.maQuocTich', '=', 'nguoinuocngoai.maQuocTich')
            ->where('maHoChieu',$ma)->orWhere('hoTen',$ten)->paginate(5);
                
            }
        }
        
        $quanly_ttkhaibao = view('nguoiql.thongtinkhaibaokdd') -> with('thongtinkhaibao',$distinct1);
        return view('layout') -> with('nguoiql.thongtinkhaibaokdd',$quanly_ttkhaibao);
    }
    public function ttcacb_edit()
    {
        
        $maCB = Session::get('nguoidung_id');
        $edit_tt = DB::table('canboquanly')
        ->join('phuongxa','canboquanly.maPhuongXa','=','phuongxa.maPhuongXa')
        ->join('quanhuyen','phuongxa.maQuanHuyen','=','quanhuyen.maQuanHuyen')
        ->join('tinhthanh','tinhthanh.maTinhThanh','=','quanhuyen.maTinhThanh')
            ->where('maCanBo', $maCB)
            ->get();
        $px = DB::table('phuongxa')->get();
        $qh = DB::table('quanhuyen')->get();
        $tt = DB::table('tinhthanh')->get();
            // echo '<pre>';
            //     print_r($edit_tt);
            //     echo '</pre>';
            //     exit;
        $manager_tt = view('nguoiql.thongtincanhannql_edit')->with('edit_tt', $edit_tt)->with('px', $px)->with('qh', $qh)
        ->with('tt', $tt);
        return view('layout')-> with('nguoiql.thongtincanhannql_edit',$manager_tt);
    }
    public function dashboard() {
        $nguoing = DB::table('nguoinuocngoai')
            ->select(DB::raw('count(*) as tong'))
            ->where('trangThaiDuyet', 'Duyệt')->get();
        //  echo '<pre>';
        //  echo($nguoing);
        //     echo '</pre>';
        //     exit;
        $chunoicutru = DB::table('chunoicutru')
            ->select(DB::raw('count(*) as tong'))
            ->where('trangThaiDuyet', 'Duyệt')->get();
        $nngcd = DB::table('nguoinuocngoai')
        ->select(DB::raw('count(*) as tong'))
        ->where('trangThaiDuyet', 'Chưa duyệt')->get();
        $cnctcd = DB::table('chunoicutru')
        ->select(DB::raw('count(*) as tong'))
        ->where('trangThaiDuyet', 'Chưa duyệt')->get();
        $kbdd = DB::table('khaibaonhapcanh')
        ->select(DB::raw('count(*) as tong'))
        ->where('trangThaiDuyet', 'Duyệt')->get();
        //  echo '<pre>';
        //  echo($kbdd);
        //     echo '</pre>';
        //     exit;
        $kbcd = DB::table('khaibaonhapcanh')
        ->select(DB::raw('count(*) as tong'))
        ->where('trangThaiDuyet', 'Chưa duyệt')->get();
        $manager_category = view('nguoiql.dashboard')->with('nguoing', $nguoing)
        ->with('chunoicutru', $chunoicutru)->with('nngcd', $nngcd)->with('cnctcd', $cnctcd)
        ->with('kbdd', $kbdd)->with('kbcd', $kbcd);
        
        return view('layout')->with('nguoiql.dashboard', $manager_category);
    }
    
}