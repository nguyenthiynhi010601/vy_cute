<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/test', function () {
    return view('test');
});
// Route::get('/','NguoiQuanLyController@index');
Route::post('/ajax/filter','trangchuController@filterAjax');
Route::get('/thongbao','NguoiQuanLyController@thongbao');
Route::post('/luuthongbao','NguoiQuanLyController@luuthongbao');
//
Route::get('/thongtinkhaibao','NguoiQuanLyController@thongtinkhaibao');
Route::get('/thongtinkhaibaocd','NguoiQuanLyController@thongtinkhaibaocd');
Route::get('/thongtinkhaibaokdd','NguoiQuanLyController@thongtinkhaibaokdd');
Route::get('/ctttkhaibao/{maHoChieu}','NguoiQuanLyController@ctttkhaibao');
Route::post('/cntrangthaiduyet/{maHoChieu}','NguoiQuanLyController@cntrangthaiduyet');
Route::post('/cntrangthaikhduyet/{maHoChieu}','NguoiQuanLyController@cntrangthaikhduyet');
Route::get('/captaikhoan/{maHoChieu}','NguoiQuanLyController@captaikhoan');
Route::post('/luutaikhoan/{maHoChieu}','NguoiQuanLyController@luutaikhoan');
//
Route::get('/thongtinnhapcanh','NguoiQuanLyController@thongtinnhapcanh');
Route::get('/thongtinnhapcanhcd','NguoiQuanLyController@thongtinnhapcanhcd');
Route::get('/ctttnhapcanh/{maXacNhan}','NguoiQuanLyController@ctttnhapcanh');
Route::post('/cntrangthaiduyetkb/{maXacNhan}','NguoiQuanLyController@cntrangthaiduyetkb');
Route::post('/cntrangthaikhduyetkb/{maXacNhan}','NguoiQuanLyController@cntrangthaikhduyetkb');
//
Route::get('/ttcacb_edit','NguoiQuanLyController@ttcacb_edit');
//
Route::get('/dangnhap','trangchuController@dangnhap');
Route::post('/kiemtra-dangnhap','trangchuController@kiemtra_dangnhap');
Route::get('/dangxuat','trangchuController@dangxuat');

//
Route::post('/timkiem','NguoiQuanLyController@timkiem');
Route::post('/timkiemcd','NguoiQuanLyController@timkiemcd');
Route::post('/timkiemkdd','NguoiQuanLyController@timkiemkdd');

//
Route::get('/homeNNG','NguoiNuocNgoaiController@homeNNG');
Route::get('/khaibaonhapcanh','NguoiNuocNgoaiController@khaibaonhapcanh');
//
Route::get('/','NguoiQuanLyController@dashboard');