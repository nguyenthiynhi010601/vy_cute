@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="{{URL::to('/thongtinkhaibao')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-layout"></i>
            <div data-i18n="Analytics">Danh sách thông tin khai báo</div>
        </a>
    </li>

    <div class="container mt-4">
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid" style="padding: 0px;">
                <div class="dropdown">
                    <button class="btn dropdown-toggle bg-white text-dark px-3 py-2" type="button"
                        id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                        Đã duyệt
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                        <li><a class="dropdown-item active" href="{{URL::to('/thongtinkhaibao')}}">Đã duyệt</a></li>
                        <li><a class="dropdown-item" href="{{URL::to('/thongtinkhaibaocd')}}">Chưa duyệt</a></li>
                        <li><a class="dropdown-item" href="{{URL::to('/thongtinkhaibaokdd')}}">Không được duyệt</a></li>
                    </ul>
                </div>
                <form action="{{URL::to('/timkiem')}}" method="post" class="d-flex">
                    {{csrf_field()}}
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="tk">
                    <button class="btn btn-outline-primary" type="submit"><i class='bx bx-search'></i></button>
                </form>

            </div>
        </nav>
        @foreach($thongtinkhaibao as $key => $thongtin)
        @if($thongtin->trangThaiDuyet == 'Duyệt')
        <div class="d-flex justify-content-between mt-3 mb-3 p-3 bg-white align-items-center rounded">
            <span>Hộ chiếu: <span class="text-dark">{{$thongtin->maHoChieu}}</span></span>
            <span>Họ tên: <span class="text-dark">{{$thongtin->hoTen}}</span></span>
            <span>Quốc tịch: <span class="text-dark">{{$thongtin->tenQuocTich}}</span></span>
            <span>Trạng thái: <span class="text-dark">{{$thongtin->trangThaiDuyet}}</span></span>
            <button type="submit" class="btn btn-outline-primary"><a
                    href="{{URL::to('/ctttkhaibao/'.$thongtin->maHoChieu)}}" class="btnhv">Xem chi
                    tiết</a></button>
        </div>
        @endif


        @endforeach
        <div>
            {{$thongtinkhaibao->render()}}
        </div>
    </div>

</div>
@endsection