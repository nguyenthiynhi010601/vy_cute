@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="#" class="menu-link">
            <i class="menu-icon tf-icons bx bx-layout"></i>
            <div data-i18n="Analytics">Duyệt thông tin nhập cảnh</div>
        </a>
    </li>
    <div class="container mt-3 py-4 px-3 bg-white rounded ">
        <div class="container" style="width: 90%;">
            @foreach($ctttnhapcanh as $key => $thongtin)
            <div class="card shadow-none bg-transparent border border-secondary mb-3">
                <p class=" fs-5 text-center alert alert-dark" role="alert">
                    <span>Mã xác nhận: <span class="text-dark">{{$thongtin->maXacNhan}}</span></span>
                </p>
                <div class="card-body ms-1">
                    <div class="row">
                        <div class="col-6">
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-user'></i>
                                    <small class="text-muted fs-6 justify-content-center">Mã hộ chiếu</small>
                                </div>
                                <div class="col-5 text-start">
                                    <h6 class="mb-0">{{$thongtin->maHoChieu}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-note'></i>
                                    <small class="text-muted fs-6 justify-content-center">Mã Visa</small>
                                </div>
                                <div class="col-5 text-start">
                                    <h6 class="mb-0">{{$thongtin->maVisa}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-calendar'></i>
                                    <small class="text-muted fs-6 justify-content-center">Ngày cấp</small>
                                </div>
                                <div class="col-5 text-start">
                                    <h6 class="mb-0">{{$thongtin->ngayCap}}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-calendar'></i>
                                    <small class="text-muted fs-6 justify-content-center">Ngày hết hạn</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->ngayHetHan}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-map'></i>
                                    <small class="text-muted fs-6 justify-content-center">Nơi cấp</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->noiCap}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-6 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-flag'></i>
                                    <small class="text-muted fs-6 justify-content-center">Ngày đi dự kiến</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->ngayDiDuKien}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-grow-1 row mb-3">
                        <div class="col-4 mb-sm-0">
                            <i class='menu-icon tf-icons bx bx-flag'></i>
                            <small class="text-muted fs-6 justify-content-center">Ngày đi thực tiễn</small>
                        </div>
                        <div class="col-9 text-start">
                            <h6 class="mb-0">{{$thongtin->ngayDiThucTe}}</h6>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-8"></div>
                        <div class="col md-auto">
                            <span>Ngày duyệt: <span class="text-dark">{{$thongtin->ngayDuyet}}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            @if($thongtin->trangThaiDuyet == 'Chưa duyệt'||$thongtin->trangThaiDuyet == 'Không được duyệt')
            <div class="d-flex justify-content-center mt-5">
                <form action="{{URL::to('/cntrangthaikhduyetkb/'.$thongtin->maXacNhan)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-outline-primary me-3">
                        Không duyệt</button>
                </form>

                <form action="{{URL::to('/cntrangthaiduyetkb/'.$thongtin->maXacNhan)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-outline-primary">
                        Duyệt</button>
                </form>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
@endsection