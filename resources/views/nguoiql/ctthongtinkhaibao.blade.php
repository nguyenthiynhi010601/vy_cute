@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="#" class="menu-link">
            <i class="menu-icon tf-icons bx bx-layout"></i>
            <div data-i18n="Analytics">Duyệt thông tin khai báo</div>
        </a>
    </li>
    <div class="container mt-3 py-4 px-3 bg-white rounded ">
        <div class="container" style="width: 90%;">
            @foreach($ctttkhaibao as $key => $thongtin)
            <div class="card shadow-none bg-transparent border border-secondary mb-3">
                <p class=" fs-5 text-center alert alert-dark" role="alert">
                    <span>Mã hộ chiếu: <span class="text-dark">{{$thongtin->maHoChieu}}</span></span>
                </p>
                <div class="card-body ms-1">

                    <div class="row mb-3">
                        <div class="col-5">
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-user'></i>
                                    <small class="text-muted fs-6 justify-content-center">Họ và tên</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->hoTen}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-note'></i>
                                    <small class="text-muted fs-6 justify-content-center">Giới tính</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->gioiTinh}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-5 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-calendar'></i>
                                    <small class="text-muted fs-6 justify-content-center">Ngày sinh</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->ngaySinh}}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-4 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-phone'></i>
                                    <small class="text-muted fs-6 justify-content-center">Số điện thoại</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->soDienThoai}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-4 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-envelope'></i>
                                    <small class="text-muted fs-6 justify-content-center">Email</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->email}}</h6>
                                </div>
                            </div>
                            <div class="flex-grow-1 row mb-3">
                                <div class="col-4 mb-sm-0">
                                    <i class='menu-icon tf-icons bx bx-flag'></i>
                                    <small class="text-muted fs-6 justify-content-center">Quốc tịch</small>
                                </div>
                                <div class="col-6 text-start">
                                    <h6 class="mb-0">{{$thongtin->tenQuocTich}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-grow-1 row mb-3">
                        <div class="col-2 mb-sm-0">
                            <i class='menu-icon tf-icons bx bx-map'></i>
                            <small class="text-muted fs-6 justify-content-center">Địa chỉ</small>
                        </div>
                        <div class="col-9 text-start">
                            <h6 class="mb-0">{{$thongtin->diaChi}}</h6>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-5"></div>
                        <div class="col md-auto">
                            <span>Ngày cập nhật: <span class="text-dark">{{$thongtin->ngayDuyet}}</span></span>
                        </div>
                        <div class="col md-auto">
                            <span>Ngày duyệt: <span class="text-dark">{{$thongtin->ngayDuyet}}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            @if($thongtin->trangThaiDuyet == 'Chưa duyệt'||$thongtin->trangThaiDuyet == 'Không được duyệt')
            <div class="d-flex justify-content-center mt-5">
                <form action="{{URL::to('/cntrangthaikhduyet/'.$thongtin->maHoChieu)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-outline-primary me-3">
                        Không duyệt</button>
                </form>

                <form action="{{URL::to('/cntrangthaiduyet/'.$thongtin->maHoChieu)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-outline-primary">
                        Duyệt</button>
                </form>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
@endsection