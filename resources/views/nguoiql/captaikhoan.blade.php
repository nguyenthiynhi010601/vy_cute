@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="{{URL::to('/captaikhoan')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-layout"></i>
            <div data-i18n="Analytics">Cấp tài khoản</div>
        </a>
    </li>
    <div class="container mt-3 py-4 px-3 bg-white rounded w-50 p-3" style="margin: 0 200px;">
        @foreach($tk as $key => $tkhoan)
        <form action="{{URL::to('/luutaikhoan/'.$tkhoan->maHoChieu)}}" method="post">
            {{csrf_field()}}
            <div class="mb-4">
                <p>Họ và tên: {{$tkhoan->hoTen}}</p>
            </div>
            <div class="mb-4">
                <label class="form-label">Tài khoản</label>
                <input type="type" class="form-control" value="{{$tkhoan->maHoChieu}}" name="taikhoan">
            </div>
            <div class="mb-4">
                <label class="form-label">Mật khẩu</label>
                <input type="password" class="form-control" name="matkhau">
            </div>


            @endforeach

            <button type="submit" class="btn btn-primary">Lưu tài khoản</button>

        </form>
    </div>



</div>
@endsection