@extends('layout')
@section('content')
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="card mb-4">
                <hr class="my-0">
                <div class="card-body">
                    <form action="" method="POST">
                        @foreach($edit_tt as $key => $thongtin)
                        <h5>Thông tin cơ bản</h5>
                        <div class="row">
                            <div class="mb-3 col-md-6">
                                <label for="firstName" class="form-label">Họ tên</label>
                                <input class="form-control" type="text" id="firstName" name="fullName"
                                    value="{{$thongtin->hoTenCB}}" autofocus="">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="lastName" class="form-label">Địa chỉ</label>
                                <input class="form-control" type="text" name="address" id="lastName"
                                    value="{{$thongtin->diaChi}}">
                            </div>

                            <div class="mb-3 col-md-6">
                                <label for="firstName" class="form-label">Ngày sinh</label>
                                <input class="form-control" type="date" id="birthday" name="birthday"
                                    value="{{$thongtin->ngaySinh}}">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label class="form-label">Phường xã</label>
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>{{$thongtin->tenPhuongxa}}</option>
                                    @foreach($px as $key => $px)
                                    <option>{{$px->tenPhuongxa}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label class="form-label" for="phoneNumber">Số điện thoại</label>
                                <div class="input-group input-group-merge">
                                    <span class="input-group-text">VN (+84)</span>
                                    <input type="text" id="phoneNumber" name="phoneNumber" class="form-control"
                                        value="{{$thongtin->soDienThoai}}">
                                </div>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label class="form-label">Quận huyện</label>
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>{{$thongtin->tenQuanHuyen}}</option>
                                    @foreach($qh as $key => $qh)
                                    <option>{{$qh->tenQuanHuyen}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="firstName" class="form-label">CCCD</label>
                                <input class="form-control" type="text" value="{{$thongtin->CCCD}}" autofocus="">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label class="form-label">Tỉnh thành</label>
                                <select class="form-select" aria-label="Default select example">

                                    <option selected>{{$thongtin->tenTinhThanh}}</option>
                                    @foreach($tt as $key => $tt)
                                    <option>{{$tt->tenTinhThanh}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">E-mail</label>
                                <input class="form-control" type="text" id="email" name="email"
                                    value="{{$thongtin->email}}">
                            </div>

                        </div>
                        @endforeach
                        <div class="mt-2">
                            <button type="submit" class="btn btn-primary me-2">Lưu</button>
                        </div>
                    </form>
                </div>
                <!-- /Account -->
            </div>
        </div>
        <!-- /Layout container -->
    </div>
    <!-- / Layout pagee -->
</div>
</div>
<!-- / Layout wrapper -->
@endsection