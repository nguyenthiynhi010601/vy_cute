@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="{{URL::to('/thongtincutru')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-layout"></i>
            <div data-i18n="Analytics">Danh sách thông tin cơ sở cư trú</div>
        </a>
    </li>

    <div class="container mt-4">
        <div class="dropdown">
            <button class="btn dropdown-toggle bg-white text-dark px-3 py-2" type="button" id="dropdownMenuButton2"
                data-bs-toggle="dropdown" aria-expanded="false">
                Đã duyệt
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                <li><a class="dropdown-item active" href="{{URL::to('/thongtinkhaibao')}}">Đã duyệt</a></li>
                <li><a class="dropdown-item" href="{{URL::to('/thongtinkhaibaocd')}}">Chưa duyệt</a></li>
            </ul>
        </div>
        <div class="d-flex justify-content-evenly mt-3 p-3 bg-white text-dark align-items-center rounded">
            <span>#1</span>
            <span>Hộ chiếu</span>
            <span>Hộ chiếu</span>
            <span>Họ tên</span>
            <span>Họ tên</span>
            <span>Thời gian</span>
            <span>Trạng thái</span>
            <button type="button" class="btn btn-primary">Xem chi tiết</button>
        </div>
        <div class="d-flex justify-content-evenly mt-3 p-3 bg-white text-dark align-items-center rounded">
            <span>#2</span>
            <span>Hộ chiếu</span>
            <span>Hộ chiếu</span>
            <span>Họ tên</span>
            <span>Họ tên</span>
            <span>Thời gian</span>
            <span>Trạng thái</span>
            <button type="button" class="btn btn-primary">Xem chi tiết</button>
        </div>
    </div>

</div>
@endsection