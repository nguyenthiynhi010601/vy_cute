@extends('layout')
@section('content')
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Thống Kê</h1>
                </div>

                <!-- Content Row -->
                <div class="row">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold mb-1">Người nước ngoài</div>
                                        @foreach($nguoing as $key => $nguoing)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$nguoing -> tong}}</div>
                                        @endforeach
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold mb-1">Khai báo nhập cảnh đã duyệt
                                        </div>

                                        @foreach($kbdd as $key => $kbdd)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$kbdd -> tong}}</div>
                                        @endforeach

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">

                                        <div class="text-xs font-weight-bold text-info mb-1">Khai báo
                                            thông tin chưa duyệt</div>

                                        @foreach($nngcd as $key => $nngcd)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$nngcd -> tong}}
                                        </div>
                                        @endforeach

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold mb-1">Chủ nơi cư trú</div>
                                        @foreach($chunoicutru as $key => $chunoicutru)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$chunoicutru -> tong}}
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info mb-1">Chủ nơi cư trú chưa duyệt
                                        </div>

                                        @foreach($cnctcd as $key => $cnctcd)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$cnctcd -> tong}}</div>
                                        @endforeach

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold mb-1">Khai báo nhập cảnh chưa duyệt
                                        </div>

                                        @foreach($kbcd as $key => $kbcd)
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$kbcd -> tong}}</div>
                                        @endforeach

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection