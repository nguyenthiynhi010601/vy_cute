@extends('layout')
@section('content')
<div>
    <li class="menu-item">
        <a href="{{URL::to('/thongbao')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-home-circle"></i>
            <div data-i18n="Analytics">Gửi Thông Báo</div>
        </a>
    </li>

    <div class="container mt-3">
        <h4 class="text-center ">THÔNG BÁO</h4>
        <form action="{{URL::to('/luuthongbao')}}" method="post">
            {{csrf_field()}}
            <div class="mb-3">
                <label class="form-label">Tên người nhận</label>
                <select class="form-select mb-4" aria-label="Default select example" name="manguoinhan">
                    @foreach($tk as $key => $tkhoan)
                    <option value="{{$tkhoan -> maHoChieu}}">{{$tkhoan -> hoTen}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label class="form-label">Tiêu đề</label>
                <input type="text" class="form-control" aria-label="default input example" name="tieude">
            </div>
            <div class="mb-3">
                <label class="form-floating">Nội dung thông báo</label>
                <textarea class="form-control" style="height: 200px" aria-label="default input example"
                    name="noidung"></textarea>

            </div>
            <p style="color: red;">
                <?php
                $msg = Session::get('msg');
                if ($msg) {
                    echo $msg;
                    Session::put('msg', null);
                }
                ?></p>
            <button type="submit" class="btn btn-outline-primary">Gửi thông báo</button>
        </form>
    </div>

</div>
@endsection