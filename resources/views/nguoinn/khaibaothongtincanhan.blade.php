@extends('homeNNG')
@section('content')
<div class="d-flex justify-content-center mt-3 ">
    <div class="">
        <h4>KHAI BÁO THÔNG TIN CÁ NHÂN CỦA NGƯỜI NƯỚC NGOÀI</h4>
        <div class="mb-3 row d-flex align-items-center mt-5">
            <label class="col-sm-3 form-label">Số hộ chiếu/CCCD</label>
            <div class="col-sm-9">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label class="col-sm-3 form-label">Họ và tên</label>
            <div class="col-sm-9">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label class="col-sm-3 form-label">Giới tính</label>
            <div class="col-sm-9">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                        value="option1">
                    <label class="form-check-label" for="inlineRadio1">Nam</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                        value="option2">
                    <label class="form-check-label" for="inlineRadio2">Nữ</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3"
                        value="option3">
                    <label class="form-check-label" for="inlineRadio3">Khác</label>
                </div>
            </div>
        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label for="firstName" class=" col-sm-3 form-label">Ngày sinh</label>
            <div class="col-sm-9">
                <input class="form-control" type="date" id="birthday" name="birthday">
            </div>

        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label class="col-sm-3 form-label">Quốc tịch</label>
            <div class="col-sm-9">
                <select class="form-select form-select" aria-label=".form-select example">
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label class="col-sm-3 form-label">Email</label>
            <div class="col-sm-9">
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="mb-3 row d-flex align-items-center">
            <label class="col-sm-3 form-label">Địa chỉ</label>
            <div class="col-sm-9">
                <input type="text" class="form-control">
            </div>
        </div>

    </div>

</div>
<div class=" d-flex justify-content-center mt-3">
    <button type="submit" class="btn btn-outline-primary">Hoàn tất</button>
</div>

@endsection