<div class="navbar navbar-expand-lg p-2 " style="background: #C2E9FB;">
    <div class="container-fluid">
        <div class="d-flex align-items-center w-50">
            <span class="demo">
                <img src="{{asset('/assets/img/avatars/logo1.png')}}" alt="logo" style="width: 65px;">
            </span>

            <div class="flex-grow-1 ms-3">
                <p class="m-0 fw-bold" style="color: #1014fd;">CÔNG AN THÀNH PHỐ ĐÀ NẴNG</p>
                <P class="m-0 fw-bold" style="color: #1014fd;">TRANG KHAI BÁO TẠM CHÚ CHO NGƯỜI NƯỚC NGOÀI</P>
            </div>
        </div>
        <div class="">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 d-flex align-items-center">
                <li class="nav-item me-2">
                    <img src="https://cdn.pixabay.com/photo/2012/04/10/23/04/vietnam-26834_640.png "
                        style="width: 60px; height:30px" alt="">
                </li>
                <li class="nav-item me-2">
                    <img src="https://vuongquocanh.com/wp-content/uploads/2018/04/la-co-vuong-quoc-anh.jpg"
                        style="width: 60px;" alt="">
                </li>
                <li class="nav-item">
                    <button type="submit" class="btn btn-outline-primary">Đăng nhập</button>
                </li>
            </ul>
        </div>
    </div>
</div>